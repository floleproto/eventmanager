﻿using Smod2.EventHandlers;
using Smod2.Events;

namespace EventManager.API
{
    internal class APIEventsHandler : IEventHandlerRoundStart, IEventHandlerRoundEnd, IEventHandlerWarheadDetonate, IEventHandlerLCZDecontaminate
    {

        private Main main;

        public APIEventsHandler(Main main)
        {
            this.main = main;
        }

        public void OnDecontaminate()
        {
            EventAPI.isDecontON = true;
        }

        public void OnDetonate()
        {
            EventAPI.isNukeExplode = true;
        }

        public void OnRoundEnd(RoundEndEvent ev)
        {
            EventAPI.isRoundStarted = false;
            EventAPI.isNukeExplode = false;
            EventAPI.isDecontON = false;
        }

        public void OnRoundStart(RoundStartEvent ev)
        {
            EventAPI.isRoundStarted = true;
            EventAPI.isNukeExplode = false;
            EventAPI.isDecontON = false;
        }
    }
}