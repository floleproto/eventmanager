﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smod2;
using Smod2.Attributes;

namespace EventManager
{
    [PluginDetails(author = "Flo - Fan", description = "EventManager", id = "flo.event", name = "Event Plugin", SmodMajor = 3, SmodMinor = 2, SmodRevision = 0, version = "0.0.1")]

    public class Main : Plugin
    {

        public override void OnDisable()
        {
            this.Info("Plugin Disabled");
        }

        public override void OnEnable()
        {
            this.Info("Plugin Enabled");

            API.EventAPI.AddEvent("spymtf");
            API.EventAPI.AddEvent("civsmtf");
            API.EventAPI.AddEvent("dvss");
            API.EventAPI.AddEvent("infect");
            API.EventAPI.AddEvent("peanutrun");
            //API.EventAPI.AddEvent("murder");
            API.EventAPI.AddEvent("049_op");
        }

        public override void Register()
        {
            this.AddEventHandlers(new Event.SpyMTF(this));
            this.AddEventHandlers(new Event.CivsMTF(this));
            this.AddEventHandlers(new Event.DvsS(this));
            this.AddEventHandlers(new Event.Infect(this));
            this.AddEventHandlers(new Event.PeanutRun(this));
            //this.AddEventHandlers(new Event.Murder(this));
            this.AddEventHandlers(new Event.OP049(this));
            this.AddEventHandlers(new Event.DeathMatch(this));

            this.AddEventHandlers(new API.APIEventsHandler(this));

            this.AddCommands(new string[] { "event", "ev" }, new Commands.EventCommands(this));
        }
    }
}
