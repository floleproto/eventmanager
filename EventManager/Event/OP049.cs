﻿using System;
using System.Timers;
using Smod2.API;
using Smod2.EventHandlers;
using Smod2.Events;
using Smod2.EventSystem.Events;

namespace EventManager.Event
{
    internal class OP049 : IEventHandlerPlayerJoin, IEventHandlerDecideTeamRespawnQueue, IEventHandlerSetSCPConfig, IEventHandlerSetRole, IEventHandlerPlayerHurt, IEventHandlerDoorAccess
    {
        private Main main;

        public OP049(Main main)
        {
            this.main = main;
        }

        public void OnDecideTeamRespawnQueue(DecideRespawnQueueEvent ev)
        {
            if (API.EventAPI.eventNameOn == "049_op")
            {
                ev.Teams = new Smod2.API.Team[] { (Smod2.API.Team)4, (Smod2.API.Team)0, (Smod2.API.Team)1, (Smod2.API.Team)3, (Smod2.API.Team)4, (Smod2.API.Team)1, (Smod2.API.Team)4, (Smod2.API.Team)4, (Smod2.API.Team)3, (Smod2.API.Team)3, (Smod2.API.Team)1, (Smod2.API.Team)4, (Smod2.API.Team)3, (Smod2.API.Team)4, (Smod2.API.Team)1, (Smod2.API.Team)3, (Smod2.API.Team)1, (Smod2.API.Team)4, (Smod2.API.Team)3 };
            }
        }

        public void OnDoorAccess(PlayerDoorAccessEvent ev)
        {
            if (API.EventAPI.eventNameOn == "049_op")
            {
                if(ev.Door.Name != "914" && ev.Door.Locked == false)
                {
                    ev.Allow = true;
                }
            }
        }

        public void OnPlayerHurt(PlayerHurtEvent ev)
        {
            if (API.EventAPI.eventNameOn == "049_op")
            {
                if (ev.DamageType == Smod2.API.DamageType.SCP_049)
                {
                    Vector v = ev.Player.GetPosition();

                    ev.Player.ChangeRole(Role.SCP_049_2, true, true, true, false);
                    Timer t = new Timer(100);
                    t.Enabled = true;
                    t.AutoReset = false;
                    t.Elapsed += delegate
                    {
                        ev.Player.Teleport(v);
                    };
                }

                if(ev.DamageType == DamageType.SCP_049_2)
                {
                    int luck = new Random().Next(0, 20);
                    if(luck == 1)
                    {
                        Vector v = ev.Player.GetPosition();
                        ev.Player.ChangeRole(Role.SCP_049_2);

                        Timer t = new Timer(100);
                        t.Enabled = true;
                        t.AutoReset = false;
                        t.Elapsed += delegate
                        {
                            ev.Player.Teleport(v);
                        };
                    }
                }
            }
        }

        public void OnPlayerJoin(PlayerJoinEvent ev)
        {
            if (API.EventAPI.eventNameOn == "049_op")
            {
                ev.Player.PersonalBroadcast(10, "<color=aqua><b>EVENT</b></color>\n" + "Un 049 est dans la partie.\n Sa particularité : être abusé. \n La partie reste normal.", true);
            }
        }

        public void OnSetRole(PlayerSetRoleEvent ev)
        {
            if (API.EventAPI.eventNameOn == "049_op")
            {
                if(ev.Player.TeamRole.Team == Smod2.API.Team.SCP)
                {
                    ev.Role = Role.SCP_049;
                }

                if (ev.Player.TeamRole.Role == Smod2.API.Role.SCP_049)
                {
                    ev.Player.SetHealth(15000);
                    ev.Player.PersonalBroadcast(10, "<color=red><b>OP 049</b></color>\n Vous êtes surcheaté. \nToutes les portes (sauf 914) ne vous résiste pas.\n Vos zombies sont soignés instantanément.", true);
                }
            }
        }

        public void OnSetSCPConfig(SetSCPConfigEvent ev)
        {
            if (API.EventAPI.eventNameOn == "049_op")
            {
                ev.Ban049 = false;

                ev.Ban079 = true;
                ev.Ban096 = true;
                ev.Ban106 = true;
                ev.Ban173 = true;
                ev.Ban939_53 = true;
                ev.Ban939_89 = true;
            }
        }
    }
}