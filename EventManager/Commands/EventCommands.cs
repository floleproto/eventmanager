﻿using Smod2.Commands;

namespace EventManager.Commands
{
    internal class EventCommands : ICommandHandler
    {
        private Main main;

        public EventCommands(Main main)
        {
            this.main = main;
        }

        public string GetCommandDescription()
        {
            return "Commande event";
        }

        public string GetUsage()
        {
            return "EV <ACTIVE | LIST | DISABLE | GET>";
        }

        public string[] OnCall(ICommandSender sender, string[] args)
        {
            if(args.Length >= 1)
            {
                if (args[0].ToLower() == "active")
                {
                    if (API.EventAPI.CheckNameEvent(args[1].ToLower()) == true)
                    {
                        API.EventAPI.ActivateEvent(args[1].ToLower());
                        return new string[] { "L'event " + args[1].ToLower() + " a été activé avec succès." };
                    }
                    return new string[] { "L'event que vous avez saisi n'existe pas." };
                }

                if (args[0].ToLower() == "list")
                {
                    string list = string.Join(" | ", API.EventAPI.eventList.ToArray());
                    return new string[] { "Liste des events disponibles", list };
                }

                if (args[0].ToLower() == "disable")
                {
                    if (args[1] == null)
                    {
                        args[1] = "lel";
                    }
                    API.EventAPI.DisableEvent();
                    return new string[] { "Event désactivé, reboot dans 10 secondes." };
                }

                if (args[0].ToLower() == "get")
                {
                    return new string[] { API.EventAPI.eventNameOn };
                }

                return new string[] { GetUsage() };
            }
            
            return new string[] { GetUsage() };
        }
    }
}